import React, {Component} from 'react';
import './App.css';
import {Provider} from './context';
import Tracks from './components/Tracks';
import Index from './components/Index';

class App extends Component {
  render(){
    return(
      <Provider>
        <React.Fragment>
          <div className="container">
          <Index />
          </div>
        </React.Fragment>
      </Provider>
    );
  }
}

export default App;
