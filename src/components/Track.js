import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import Modal from 'react-modal';
import {Button} from 'reactstrap';
import { BrowserRouter } from 'react-router-dom';

class Track extends Component {
  constructor(props) {
    super(props);

    this.state = { isOpen: false };
  }

  toggleModal = () => {
     this.setState({
       isOpen: !this.state.isOpen
     });
     console.log(this.state);
   }

render(){
  const {track} = this.props;
  return(
    <React.Fragment>
    <div className="col-md-6 select-item">
      <div className="card mb-4 shadow-sm">
        <div className="card-body">
          <h5>{track.artist_name}</h5>
          <p className="card-text">
            <strong><i className="fas fa-play"></i> Track</strong>: {track.track_name}
            <br/>
            <strong><i className="fas fa-compact-disc"></i> Album</strong>: {track.album_name}
          </p>
        </div>
      </div>
    </div>

    <Modal>
          Here's some content for the modal
        </Modal>
      </React.Fragment>
  );
}

}

export default Track;
